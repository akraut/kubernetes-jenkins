# Kubernetes Jenkins

**Problem:**

To build the LCG software stack provided by the SPI section in the EP-SFT group, we need lots of CPU and RAM from midnight until about 10 am. Unfortuntately, nowadays we are limited by the number of Jenkins worker nodes (puppetized VMs) as Jenkins worker nodes.

**Approach:**

By using Kurbernetes as a tool to orchestrate the resource usage we want to make a better use of our quota and virtualize our resources. Also, Kubernetes can be used by several Jenkins masters as opposed to the classical approach beased on VMs.

**Documentation:**

 - [CERN Cloud and Containers](http://clouddocs.web.cern.ch/clouddocs/containers)
 - [OpenStack Magnum Docs](https://docs.openstack.org/magnum/latest/user/)

**Support:**

 - There's a group in CERN's Mattermost chat called [Cloud Containers](https://mattermost.web.cern.ch/it-dep/channels/cloud-containers)
 - The IT contact is [Ricardo Rocha](mailto:Ricardo.Rocha@cern.ch)


## Getting started

 - You can instantiate your own kubernetes cluster following the instructions in the [CloudDocs](http://clouddocs.web.cern.ch/clouddocs/containers/quickstart.html)
 - For larger clusters, you can ask to [extend the quota on the project](http://clouddocs.web.cern.ch/clouddocs/details/quotas.html)

Download and save your personal OpenStack credential from [CERN's OpenStack web interface](https://openstack.cern.ch/project/api_access/). Make sure you have selected the correct project from the dropdown menu on the top left before. Click on *DONWLOAD OPENSTACK RC FILE* and select *OPENSTACK RC FILE (IDENTITY API V3)*. Upload it to `lxplus-cloud` like 
```
scp ~/Downloads/PH\ LCGAA-openrc.sh lxplus-cloud.cern.ch:~/lcgapp-openstack-rc.sh
```

The **lxplus cloud** environment has the latest `openstack` and `kubectl` software installed.
```
ssh lxplus-cloud.cern.ch 
```

On `lxplus-cloud` load the configuration with
```
source ~/lcgapp-openstack-rc.sh
```

Create a project specific SSH key for OpenStack:
```
openstack keypair create lcgapp > ~/.ssh/lcgapp
chmod 600 ~/.ssh/lcgapp
```

IT currently recommends to use the cluster template `kubernetes-1.13.3-1` since they introduced a versioned template in March 2019.

Size (so called flavor) of the nodes:
 - As a first step we will deploy only a few worker minions, so the master node doesn't need to be very big
 - According to [this recommendation](https://kubernetes.io/docs/setup/cluster-large), the flavour `	m2.medium` for the master node should be enough;
 - As far as the worker nodes are concerned, the same flavour as the previous VMs is resonable: `m2.xlarge`

As [Ricardo Rocha](mailto:Ricardo.Rocha@cern.ch), the responsible for Kubernetes in IT, is pointing out it is currentlty only possible to overwrite labels from the template (such as flavor) if you replace all labels. That's why the list of labels below is so vast.

Create a kubernetes cluster with 3 worker nodes and CVMFS enabled on CERN's OpenStack infrastructure like this:
```
openstack coe cluster create lcgapp-kubernetes \
    --keypair lcgapp \
    --cluster-template kubernetes-1.13.3-1 \
    --master-flavor m2.medium \
    --node-count 3 --flavor m2.xlarge \
    --labels admission_control_list=NamespaceLifecycle,LimitRanger,ServiceAccount,DefaultStorageClass,DefaultTolerationSeconds,MutatingAdmissionWebhook,ValidatingAdmissionWebhook,ResourceQuota,Priority \
    --labels cephfs_csi_enabled=True \
    --labels cephfs_csi_version=v0.3.0 \
    --labels cern_enabled=true \
    --labels cgroup_driver=cgroupfs \
    --labels container_infra_prefix=gitlab-registry.cern.ch/cloud/atomic-system-containers/ \
    --labels cvmfs_csi_enabled=True \
    --labels cvmfs_csi_version=v0.3.0 \
    --labels cvmfs_tag=qa \
    --labels flannel_backend=vxlan \
    --labels influx_grafana_dashboard_enabled=True \
    --labels ingress_controller=traefik \
    --labels kube_csi_enabled=True \
    --labels kube_csi_version=v0.3.2 \
    --labels kube_tag=v1.13.3-12 \
    --labels manila_enabled=True \
    --labels manila_version=v0.3.0
```

Now you'll have to wait quite some time. In this case it takes about one hour for the cluster setup to complete.

Verify the creation of the cluster:
```
openstack coe cluster list
```

Save the configuration for later replication:
```
openstack coe cluster config lcgapp-kubernetes > ~/lcgapp-kubernetes-env.sh
```

The **Kubernetes API** is available on the master node on port 6443 (using the HTTPS protocol):
```
https://<master-node>:6443
```

## Create a service account

In order for Jenkins to be able to access the Kubernetes API you need to create a service account. It needs to have enough permissions to accomplish all necessary tasks but on the other side it makes sense to limit the permissions as much as possible.

```
TODO: Permission set for service account
``` 

Store the API key of the service account in Jenkins in the Credentials section as a *secret text* to use it later for the Kubernetes plugin.


## Add Kubernetes to Jenkins

Our new Jenkins CI server can be reached under this URL: [https://lcgapp-services.cern.ch/jenkins/](https://lcgapp-services.cern.ch/jenkins/).

Please install the [Kubernetes Plugin](https://plugins.jenkins.io/kubernetes) as described [here](https://github.com/jenkinsci/kubernetes-plugin/blob/master/README.md). Please note that the Credentials type `Kubernetes Service Account` is only available if you run Jenkins inside the Kubernetes cluster as well - which is not the case for our setup in EP-SFT. That's why we needed to create a Service Account in the previous step.

### Configuration

Go to **Manage Jenkins** > **Configure System** > Scroll to the **Cloud** section and click on **Add a new cloud** > **Kubernetes**


## CVMFS support

Please note that the label `cvmfs_storage_driver` is an experimental feature for Docker Swarm that doesn't need to be enabled for CVMFS support in Kubernetes. For that reason we have a CSI driver (Container Storage Interface) for read-only access:
 - [Tutorial](http://clouddocs.web.cern.ch/clouddocs/containers/tutorials/cvmfs.html#kubernetes)
 - [GitLab: CVMFS CSI driver](https://gitlab.cern.ch/cloud-infrastructure/cvmfs-csi)


## Logging

Since Kubernetes jobs are ephemeral, there needs to be a solution to store the log files. There are two approaches:

### Keep old jobs

There's an option to keep old jobs in the kubernetes cluster for n hours/days/months. This can be tweaked to fin the best compromise between disk space usage and reproducability.

### ElasticSearch

There's a `fluentd` based solution that sends log files to an ElasticSearch cluster in IT with Kibana as a frontend.

